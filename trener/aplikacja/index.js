require('dotenv').config({})
const express = require("express");
const config = require("./config");
const mongo = require('./config/mongo');
const { admin, pages, api, files } = require("./routes");

const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 3000;

var app = express();

config(app)

app.use('/files', files)
app.use('/admin', admin)
app.use('/pages', pages)
app.use('/strony', pages)
app.use('/api', api)

app.get("/", function (req, res) {
    res.redirect('/pages/')
});

// Custom error handler middleware
// app.use((error, req, res, next) => {
//     res.render('layout/error')
// })

mongo.connect().then(() => {
    console.log('Connected to mongodb');

    const server = app.listen(PORT, HOST, () => {
        console.log(`Server is Listening on http://${HOST}:${PORT}/`);
    });
})


