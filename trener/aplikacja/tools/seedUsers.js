let fs = require('fs')
let path = require('path')
const { pipeline } = require('stream')
let { createGzip } = require('zlib')
let { parse } = require('csv-parse')


async function main() {
    const readFilePath = '../../dane/FakeNameGenerator.com_c12c3c65.csv'
    // const writeFilePath = '../../dane/FakeNameGenerator.com_copy.csv.zip'
    const writeFilePath = '../../dane/FakeNameGenerator.com_copy.json'

    const zip = createGzip()


    const file = fs.createReadStream(readFilePath)//, { highWaterMark: 100 })
    const output = fs.createWriteStream(writeFilePath)//, { highWaterMark: 50 })

    // file.pipe(parse).pipe(output)

    let index = 0
    const pipe = pipeline(
        file,
        parse({
            delimiter: ',',
            autoParse: true,
            columns: true,
            toLine: 10
        }),
        // output,
        (err) => {
            if (err) { console.error(err); return; }
            console.log('Success');
        }
    )
    // .on('data', (row) => {
    //     console.log(row);       
    // })

    for await (let row of pipe) {
        console.log(row);
    }

    // // Flowing
    // file.on('data', (chunk) => {
    //     console.log(chunk)
    //     const canISendMore = output.write(output)

    //     file.pause()
    // })

    // output.on('drain',()=>{
    //     file.resume()
    // })

    // // Paused 
    // file.on('readable', () => {
    //     // const chunk = file.read()
    //     file.resume()
    // })

    // file.on('end', () => {

    // })

}

main()



// Number
// Gender
// NameSet
// Title
// GivenName
// MiddleInitial
// Surname
// StreetAddress
// City
// State
// StateFull
// ZipCode
// Country
// CountryFull
// EmailAddress
// Username
// Password
// BrowserUserAgent
// TelephoneNumber
// TelephoneCountryCode
// MothersMaiden
// Birthday
// Age
// TropicalZodiac
// CCType
// CCNumber
// CVV2
// CCExpires
// NationalID
// UPS
// WesternUnionMTCN
// MoneyGramMTCN
// Color
// Occupation
// Company
// Vehicle
// Domain
// BloodType
// Pounds
// Kilograms
// FeetInches
// Centimeters
// GUID
// Latitude
// Longitude

// 1,female,American,Mrs.,Amber,B,Harris,"4299 Augusta Park",Union,WV,"West Virginia",24983,US,"United States",AmberBHarris@dayrep.com,Whirs1988,kahmieYah5u,"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36",304-772-6781,1,Larsen,6/13/1988,32,Gemini,MasterCard,5238351499425262,821,11/2024,235-04-0059,"1Z W62 955 59 4612 472 6",8755781205,31352855,Orange,"Administrative specialist","The Polka Dot Bear Tavern","2002 Audi A8",tuajhfv.com,B+,176.7,80.3,"5' 9""",174,22b020f1-6798-4f97-b60f-e3a9b2d152ca,37.578622,-80.568627
