const os = require('os');
const { getArgs } = require("./extractArgs");

console.log("Witaj w moim CLI:");
console.log("Dostepne opcje:");
console.log("--param [wartosc]");
console.log("--option [wartosc]");

// Serialize to string:
// console.log(JSON.stringify(process.argv, null, 2))

// Destructuring
const [nodePath, scriptPath, ...restArgs] = process.argv;
console.log(restArgs);

// Function Hoisting
const args = getArgs(restArgs);

for(let option of args){
    console.log(`${option.key}: ${option.value}`);
}


// Array methods
// console.log(process.argv.slice(2))

console.log(`Script Path: 
    ${scriptPath}`)
console.log(`Current working dir: 
    ${process.cwd()} `);
console.log(`Dirname: 
    ${__dirname} `);
console.log(`Filename: 
    ${__filename} `);

console.log("Processor: " + process.arch);

// Template strings " ` " - backtick
console.log(`Node path: 
    "${process.execPath}" `);

console.log(`CPU: 
    ${process.cpuUsage().user / 1024} KB`);
console.log(`Mem: 
    ${process.memoryUsage().heapUsed / 1024} KB`);


// Stop script from continuing
// process.exit(1)

module.exports = {}