```js



console.log(x)

closureFn()

// hoisting
function closureFn(){
    console.log(x)  
    var y = 2
    closedFn()

    // hoisting to 'closureFn'
    function closedFn(){
        console.log(x)  
        console.log(y)  
        debugger;
    }  
    
}

// hoisting
var x = 1

closureFn()

/// =======


x 
VM648:1 Uncaught ReferenceError: x is not defined
console.log(x)

let x = 1;
VM696:1 Uncaught ReferenceError: x is not defined
x 

VM706:1 Uncaught ReferenceError: x is not defined

let x = 1;
x 
1

/// =======


console.log(z) 

if(false){
    var z = 123
}

console.log(z) 
undefined

console.log(q) 

/// =======

if(false){
    let q = 123
}

console.log(q) 
VM860:3 Uncaught ReferenceError: q is not defined

/// =======


if(true){
    let q = 123
}

console.log(q) 
VM877:7 Uncaught ReferenceError: q is not defined

/// =======

{
    let q = 123
}

console.log(q) 
VM886:5 Uncaught ReferenceError: q is not defined

/// =======

p = 123
123
window.p 
123
p 
123

/// ===== CONST

const obj1 = { value: 123 }
obj1 = 123
// Uncaught TypeError: Assignment to constant variable.

obj1.value = 'abc'
// 'abc'

obj1 
// {value: 'abc'}

{
    const obj2 = { value: 123 }
}
obj2
// VM1108:4 Uncaught ReferenceError: obj2 is not defined

Object.freeze(obj1)
// {value: 'abc'}

obj1.value = 'abc123'
// 'abc123'

obj1 
// {value: 'abc'}

```

