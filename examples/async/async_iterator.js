myInfiniteIterator = {
    value: 0,
    max: 100,
    [Symbol.asyncIterator]: function () {
        return {
            next: () => new Promise(resolve => {
                setTimeout(() => {
                    resolve({ value: this.value++, done: this.max <= this.value })
                }, 1000)
            }),
        };
    },
}

// it = myInfiniteIterator[Symbol.iterator]()
// it.next()

myInfiniteIterator.max = 10;

for await (let i of myInfiniteIterator) {
    console.log(i);

    //     if(i> 10) break;
}