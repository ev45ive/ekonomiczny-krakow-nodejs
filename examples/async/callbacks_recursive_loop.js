// Nested Callbacks, Recrusive Loops


asyncLoop = (data, callback)=>{
    
    const callback = (err, res)=>{
        if(err){ callback(err); return;}

        if( finished ){
            // sucees
            callback(null, finished)
        }else{
            myAsyncFn(param++, callback)
        }
    }
    myAsyncFn(param, callback)
}

myAsyncFn = (param, callback) => {
      async(param, (err, res)=>{       
        if(err){ callback(err); return;}  

        async(param, (err, res)=>{
            if(err){ callback(err); return;}
            callback(err,res)
        })
    })
}