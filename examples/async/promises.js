echo = (msg) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(msg)
        }, 1000)
    });
}

p = echo('Ala')
    .then(res => res + ' ma ')
    .then(res => {
        return echo(res + ' kota')
    })

p.then(res => {
    console.log(1, res)
})

p.then(res => {
    console.log(2, res)
})

setTimeout(() => {
    // later ...
    p.then(res => {
        console.log(3, res)
    })
}, 4000)

/// ========== Error Handling ==============

echo = (msg, fakeErr) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            fakeErr ? reject(fakeErr) : resolve(msg)
        }, 1000)
    });
}

p = echo('Ala', 'upps',)
    //      .catch(err => 'Anonim ')
    .catch(err => { throw 'no user ' })
    .then(res => res + ' ma ')
    .then(res => {
        return echo(res + ' kota')
    })
    .catch(err => {
        console.error('err', err);
        return 'No data'
    })

p.then(res => {
    console.log(1, res)
})

p.then(res => {
    console.log(2, res)
})

setTimeout(() => {
    // later ...
    p.then(res => {
        console.log(3, res)
    })
}, 4000)


// p = echo('Ala')    

// p2 = p.then( res => res + ' ma ')
// p3 = p2.then( res => res + ' kota')

// p3.then( res => {
//   console.log(res)
// })


console.log('request sent')
// VM2196:47 request sent
// undefined
// VM2196:17 err no user 
// (anonymous) @ VM2196:17
// Promise.catch (async)
// (anonymous) @ VM2196:16
// VM2196:22 1 'No data'
// VM2196:26 2 'No data'
// VM2196:32 3 'No data'

///// ==========

p1 = echo('Post 1')
p2 = echo('Post 2')
p3 = echo('Post 3')

posty = Promise.all([p1, p2, p3])

posty.then( res => res == ['Post 1', 'Post 2', 'Post 3'])