
const echo = (msg, fakeErr) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            fakeErr ? reject(fakeErr) : resolve(msg)
        }, 1000)
    });
}


async function main() {
    try {
        const user = await echo('Ala')
        const pet = await echo('kota')
        // const pet2 = await Promise.resolve('i rybki')
        // const pet2 = await Promise.rejected('i bład')

        return `${user} ma ${pet}`
    } catch (err) {
        return 'Cant load ' + err
    }
}

main().then(console.log)

// const { promisify } = require("util");

promisify2 = function (funcWithCallback) {
    return function (...args) {
        return new Promise((resolve, reject) => {
            funcWithCallback.apply(this, [...args, (err, result) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(result)
                }
            }])
        });

    }
}



async function main() {
    try {
        const ids = [1, 2, 3]

        const postPromisesArr = ids.map(id => echo('Post ' + id))

        const posts = await Promise.all(postPromisesArr)

        return posts;

    } catch (err) {
        return 'Cant load ' + err
    }
}


await main()
// (3) ['Post 1', 'Post 2', 'Post 3']