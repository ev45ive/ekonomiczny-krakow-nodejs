const http = require("http");

const hostname = "127.0.0.1";
const port = process.env.PORT || 9000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    res.end("Hello World!\n");
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});

server.on('error', console.log)

// node ./index.js 
// Server running at http://127.0.0.1:9000/

// node index.js
// Error: listen EADDRINUSE: address already in use 127.0.0.1:8080
//     at Server.setupListenHandle [as _listen2] (node:net:1334:16)
//     at listenInCluster (node:net:1382:12)
//     at doListen (node:net:1520:7)
//     at processTicksAndRejections (node:internal/process/task_queues:84:21) {
//   code: 'EADDRINUSE',
//   errno: -4091,
//   syscall: 'listen',
//   address: '127.0.0.1',
//   port: 8080
// }